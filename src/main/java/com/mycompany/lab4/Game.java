/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author fkais
 */
public class Game {
    private Player player1, player2;
    private Table board;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void Play() {
        boolean isFinish= false;
        newGame();
        while (!isFinish) {
            printBoard();
            printTurn();
            inputRowCol();
            if (board.checkWiner()) {
                printBoard();
                printWinner();
                printPlayers();
                if(Playagain()){
                    board.resetBoard();
                    continue;
                } 
                isFinish= true;
            }
            if (board.checkDraw()) {
                printBoard();
                printDraw();
                printPlayers();
                if(Playagain()){
                    board.resetBoard();
                    continue;
                } 
                isFinish= true;
            }
            board.switchPlayer();
        }
        
    }
    
        private boolean Playagain() {
        String answer;
        System.out.println("Play again? (y/n)");
        Scanner sc = new Scanner(System.in);
        answer = sc.next();
        if(answer.equals("y")){
            return true;
        }else if(answer.equals("n")){
            return false;
        }
        return false;
    }
    
    private void printBoard() {
        char[][] b = board.getBoard();
         for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(b[i][j] + " ");
            }
            System.out.println("");
        }
    }
    

    private void printTurn() {
        System.out.println(board.getCurrentPlayer().getSymbol() + " : turn");
    }
    
    private void newGame() {
       board = new Table(player1,player2);
    }
    
    private void printWinner() {
        System.out.println(board.getCurrentPlayer().getSymbol() + " ");
    }
    
    private void printDraw() {
        System.out.println("Draw!!!");
    }
    
    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);
    }
    
    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row,col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        board.setRowCol(row, col);

    }

}
